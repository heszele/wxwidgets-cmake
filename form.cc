///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "form.hh"
#include <wx/dcclient.h>

class open_gl_canvas_type: public wxGLCanvas
{
public:
    open_gl_canvas_type(wxFrame* parent):
        wxGLCanvas(parent)
    {
        if (glewInit() != GLEW_OK)
        {
            // TODO: Error handling
        }
        _open_gl_context = new wxGLContext(this);
        if(!_open_gl_context->IsOK())
        {
            // TODO: Error handling
        }
        Bind(wxEVT_PAINT, &open_gl_canvas_type::on_paint, this);
    }

    void on_paint(wxPaintEvent&)
    {
        render();
    }

    void on_size(wxSizeEvent& event)
    {

    }

    void render()
    {
        SetCurrent(*_open_gl_context);
        wxPaintDC(this);
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);
        glViewport(0, 0, (GLint)GetSize().x, (GLint)GetSize().y);

        glBegin(GL_POLYGON);
            glColor3f(1.0, 1.0, 1.0);
            glVertex2f(-0.5, -0.5);
            glVertex2f(-0.5, 0.5);
            glVertex2f(0.5, 0.5);
            glVertex2f(0.5, -0.5);
            glColor3f(0.4, 0.5, 0.4);
            glVertex2f(0.0, -0.8);
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(1.0, 0.0, 0.0);
            glVertex2f(0.1, 0.1);
            glVertex2f(-0.1, 0.1);
            glVertex2f(-0.1, -0.1);
            glVertex2f(0.1, -0.1);
        glEnd();

    // using a little of glut
        // glColor4f(0,0,1,1);
        // glutWireTeapot(0.4);

        // glLoadIdentity();
        // glColor4f(2,0,1,1);
        // glutWireTeapot(0.6);
    // done using glut

        glFlush();
        SwapBuffers();
    }

protected:
    DECLARE_EVENT_TABLE()

private:
    wxGLContext* _open_gl_context = nullptr;
};

BEGIN_EVENT_TABLE(open_gl_canvas_type, wxGLCanvas)
    EVT_PAINT    (open_gl_canvas_type::on_paint)
    EVT_SIZE(open_gl_canvas_type::on_size)
END_EVENT_TABLE()

///////////////////////////////////////////////////////////////////////////

main_frame_type::main_frame_type( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* _main_sizer;
	_main_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* _vertical_sizer;
	_vertical_sizer = new wxBoxSizer( wxHORIZONTAL );

    _open_gl_canvas = new open_gl_canvas_type(this);
	// _render_view = new wxButton( this, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	_vertical_sizer->Add( _open_gl_canvas, 1, wxALL|wxEXPAND, 5 );

	_scene_tree_view = new wxTreeCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE );
	_vertical_sizer->Add( _scene_tree_view, 1, wxALL|wxEXPAND, 5 );

	_properties_tree_view = new wxTreeCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE );
	_vertical_sizer->Add( _properties_tree_view, 1, wxALL|wxEXPAND, 5 );


	_main_sizer->Add( _vertical_sizer, 1, wxEXPAND, 5 );

	_asset_browser = new wxButton( this, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	_main_sizer->Add( _asset_browser, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( _main_sizer );
	this->Layout();

	this->Centre( wxBOTH );
}

main_frame_type::~main_frame_type()
{
}
